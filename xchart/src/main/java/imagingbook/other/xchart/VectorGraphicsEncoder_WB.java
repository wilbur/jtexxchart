/**
 * Copyright 2015 Knowm Inc. (http://knowm.org) and contributors.
 * Copyright 2011-2015 Xeiam LLC (http://xeiam.com) and contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package imagingbook.other.xchart;

import java.io.FileOutputStream;
import java.io.IOException;

import org.scilab.forge.jlatexmath.ext.MathRendering;

import de.erichseifert.vectorgraphics2d.EPSGraphics2D;
import de.erichseifert.vectorgraphics2d.PDFGraphics2D;
//import de.erichseifert.vectorgraphics2d.ProcessingPipeline;
import de.erichseifert.vectorgraphics2d.SVGGraphics2D;
import de.erichseifert.vectorgraphics2d.VectorGraphics2D;


/**
 * A helper class with static methods for saving Charts as bitmaps
 * 
 * @author timmolter
 */
public final class VectorGraphicsEncoder_WB {
	
	public static boolean RENDER_TEXT_OUTLINE = true;	// wilbur: added 

	/**
	 * Constructor - Private constructor to prevent instantiation
	 */
	private VectorGraphicsEncoder_WB() {
	}

	public enum VectorGraphicsFormat {
		EPS, PDF, SVG;
	}
	
	public static void saveVectorGraphic(Chart chart, String fileName, VectorGraphicsFormat vectorGraphicsFormat) throws IOException {
		VectorGraphics2D g = null;	
		//ProcessingPipeline g = null; // wilbur: required for newer version  (for VectorGraphics2D unreleased 2015-12-24)
		switch (vectorGraphicsFormat) {
		case EPS:
			g = new EPSGraphics2D(0.0, 0.0, chart.getWidth(), chart.getHeight());
			break;
		case PDF:
			g = new PDFGraphics2D(0.0, 0.0, chart.getWidth(), chart.getHeight());
			break;
		case SVG:
			g = new SVGGraphics2D(0.0, 0.0, chart.getWidth(), chart.getHeight());
			break;
		default:
			break;
		}
		
		if (RENDER_TEXT_OUTLINE) {
			g.setRenderingHint(MathRendering.KEY_RENDER_TEXT, MathRendering.VALUE_RENDER_TEXT_OUTLINE);	//wilbur: check!
			//g.setRenderingHint(VectorHints.KEY_TEXT, VectorHints.VALUE_TEXT_VECTOR);  // for newer version, currently ignored
		}
		
		chart.paint(g, chart.getWidth(), chart.getHeight());

		// Write the vector graphic output to a file
		FileOutputStream strm = new FileOutputStream(fileName + "." + vectorGraphicsFormat.toString().toLowerCase());
		try {
			strm.write(g.getBytes());
			//g.writeTo(strm); // wilbur: required for newer version  (for VectorGraphics2D unreleased 2015-12-24)
		} finally {
			strm.close();
		}
	}

	// original
//	public static void saveVectorGraphic(Chart chart, String fileName, VectorGraphicsFormat vectorGraphicsFormat) throws IOException {
//		VectorGraphics2D g = null;
//		switch (vectorGraphicsFormat) {
//		case EPS:
//			g = new EPSGraphics2D(0.0, 0.0, chart.getWidth(), chart.getHeight());
//			break;
//		case PDF:
//			g = new PDFGraphics2D(0.0, 0.0, chart.getWidth(), chart.getHeight());
//			break;
//		case SVG:
//			g = new SVGGraphics2D(0.0, 0.0, chart.getWidth(), chart.getHeight());
//			break;
//		default:
//			break;
//		}
//		
//		chart.paint(g, chart.getWidth(), chart.getHeight());
//
//		// Write the vector graphic output to a file
//		FileOutputStream file = new FileOutputStream(fileName + "." + vectorGraphicsFormat.toString().toLowerCase());
//		try {
//			file.write(g.getBytes());
//		} finally {
//			file.close();
//		}
//	}
	
	


}
