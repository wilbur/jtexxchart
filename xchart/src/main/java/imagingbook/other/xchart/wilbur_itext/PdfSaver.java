package imagingbook.other.xchart.wilbur_itext;

import java.io.FileNotFoundException;

import imagingbook.other.xchart.Chart;

/**
 * Super class for the PDF savers. Extend this class to
 * add a new PDF saver.
 * @author wilbur
 *
 */
public abstract class PdfSaver {
	
	protected final boolean renderTextOutline;
	
	protected PdfSaver(boolean renderTextOutline) {
		this.renderTextOutline = renderTextOutline;
	}
	
	public abstract String save(Chart chart, String path) throws FileNotFoundException;

}
