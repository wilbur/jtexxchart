package imagingbook.jtexxchart.examples;

import imagingbook.other.xchart.Chart;

public interface ExampleChart {
	
	  public Chart getChart();

}
