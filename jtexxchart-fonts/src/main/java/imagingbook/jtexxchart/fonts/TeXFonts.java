package imagingbook.jtexxchart.fonts;

import java.awt.Font;

/**
 * TODO: make font cache?
 * @author wilbur
 *
 */
public class TeXFonts {
	
	public enum FontFamily {
		rm("cmunrm.otf"), 
		bf("cmunrb.otf"),
		it("cmunci.otf"),
		tt("cmuntt.otf"),
		sf("cmunss.otf");
		
		protected final String filename;
		private FontFamily(String filename) {
			this.filename = filename;
		}
	}
	
	static final String RelativeFontPath = "cmunicode/otf/";
	
	public static Font getFont(FontFamily family, float size) {
		Font f = Fonts.getFont(RelativeFontPath, family.filename);
		return f.deriveFont(size);
	}
	
	public static void main(String[] args) {
		float size = 12;
		for (FontFamily fam : FontFamily.values()) {
			Font f = getFont(fam, size);
			System.out.println(fam.name() + " : " + f);
		}
	}

}
