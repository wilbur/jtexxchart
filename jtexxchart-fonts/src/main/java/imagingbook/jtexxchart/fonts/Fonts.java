package imagingbook.jtexxchart.fonts;

import java.awt.Font;
import java.io.InputStream;
import java.nio.file.Path;

/**
 * To test with a JAR file:
 * cd C:\SVN_JTexXchart_SourceForge\trunk\jtexxchart-fonts\target
 * java -classpath jtexxchart-fonts-0.0.1-SNAPSHOT.jar imagingbook.jtexxchart.fonts.Fonts
 * @author wilbur
 *
 */
public class Fonts {
	
	static final String RelativeFontPath = "cmunicode/otf/";
	
	public static String[] getAllFontFileNames(String relPath) {
		Path[] paths = FileUtils.listResources(Fonts.class, relPath);
		String[] filenames = new String[paths.length];
		for (int i = 0; i < paths.length; i++) {
			filenames[i] = paths[i].getFileName().toString();
		}
		return filenames;
	}
	
	public static Font getFont(String relativePath, String fontFileName) {
		InputStream strm = FileUtils.getResourceStream(Fonts.class, relativePath + fontFileName);
		if (strm == null)
			return null;
		else {
			return FileUtils.getFontFromStream(strm);
		}
	}

	
	public static void main(String[] args) {
		Font f1 = getFont(RelativeFontPath, "cmunbbx.otf");
		System.out.println("f = " + f1);
		Font f12 = f1.deriveFont((float)12);
		System.out.println("f12 = " + f12 + " size = " + f12.getSize());
		
		String[] filenames = getAllFontFileNames(RelativeFontPath);
		System.out.println("listing files:");
		
		int i = 0;
		for (String s : filenames) {
			i++;
			System.out.println(i + " file = " + s);
		}
	}
}
