package org.scilab.forge.jlatexmath.ext;

import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 * Defines hints to control the rendering of characters in
 * mathematical formulas. Since some vector graphics libraries for
 * exporting SVG, EPS, or PDF files (e.g., VectorGraphics2D)
 * do not support proper font embedding, math text should be
 * drawn as filled outline shapes.
 * 
 * These rendering hints can be applied to any Graphics2D object. They are checked 
 * during rendering in class {@link org.scilab.forge.jlatexmath.CharBox}.
 * 
 * @author wilbur
 * @version 2015/12/28
 *
 */
public abstract class MathRendering {
	
	/**
	 * Use to hint the associated {@link Graphics2D} instance to
	 * render math texts as either outline shapes or font glyphs
	 * (provided the specific {@link Graphics2D} honors the hint).
	 * 
	 * @param g2 graphics environment
	 * @param outline pass {@code true} to get outline rendering, {@code false} for glyph rendering
	 */
	public static void setRenderOutline(Graphics2D g2, boolean outline) {
		g2.setRenderingHint(MathRendering.KEY_RENDER_TEXT,
				outline ? 
					MathRendering.VALUE_RENDER_TEXT_OUTLINE : 
					MathRendering.VALUE_RENDER_TEXT_GLYPHS);
	}
	
	/**
	 * To be used in the render process to check if outline or glyph rendering should be applied. 
	 * Used in class {@link org.scilab.forge.jlatexmath.CharBox}, for example.
	 * 
	 * @param g2 graphics environment
	 * @return {@code true} if outline rendering is intended, {@code false} otherwise
	 */
	public static boolean shouldRenderOutline(Graphics2D g2) {
		return g2.getRenderingHint(KEY_RENDER_TEXT) == VALUE_RENDER_TEXT_OUTLINE;
	}
	
	// -----------------------------------------------------------
	
	public static final Key KEY_RENDER_TEXT = new Key(101);
	private static final Value VALUE_RENDER_TEXT_GLYPHS  = new Value(KEY_RENDER_TEXT);
	public static final Value VALUE_RENDER_TEXT_OUTLINE = new Value(KEY_RENDER_TEXT);

	/**
	 * An inner class defining rendering hint keys, modeled after SunHints.java (JDK7).
	 */
	public static class Key extends RenderingHints.Key {

		public Key(int privatekey) {
			super(privatekey);
		}

        public boolean isCompatibleValue(Object val) {
        	return (val instanceof Value && ((Value)val).key == this);
        }
        
//        public final int getIndex() {
//            return intKey();
//        }
	}
	
    public static class Value {
    	private final Key key;	// the key associated with this value
    	
        public Value(Key key) {
        	this .key = key;
        }
    }

}
