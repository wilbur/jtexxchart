
/**
 * This package holds extensions to the original jlatexmath distribution.
 * 
 * @author W. Burger (wilbur)
 *
 */
package org.scilab.forge.jlatexmath.ext;