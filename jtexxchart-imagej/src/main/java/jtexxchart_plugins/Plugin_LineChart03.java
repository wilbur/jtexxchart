package jtexxchart_plugins;
import java.util.Locale;

import ij.IJ;
import ij.plugin.PlugIn;
import imagingbook.jtexxchart.examples.LineChart03;
import imagingbook.jtexxchart.imagej.IjSwingWrapper;
import imagingbook.lib.ij.IjLogStream;
import imagingbook.other.xchart.Chart;


public class Plugin_LineChart03 implements PlugIn {
	
	static {
		IjLogStream.redirectSystem();
		Locale.setDefault(Locale.US);
	}

	@Override
	public void run(String arg0) {
		IJ.log("starting");	// THERE MUST BE SOME OUTPUT, otherwise plugin stalls on launch!!! - problem with output stream!!!
		Chart chart =  new LineChart03().getChart();
		new IjSwingWrapper(chart).displayChart();
	}

}
